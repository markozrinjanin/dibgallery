
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Album } from '../models';

@Injectable({ providedIn: 'root' })
export class AlbumService {
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Album[]>(`/albums`);
  }

}
