import { Album, Image } from './../../../models/albums';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { User } from '../../../models';
import { AuthenticationService } from '../../../services';
import { Router } from '@angular/router';

@Component({ templateUrl: 'image.component.html', styleUrls: ['image.component.scss'] })
export class ImageComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    layout = 'grid';
    images: Image[] = [];
    private currentAlbumSubject: BehaviorSubject<Album>;
    private currentImageSubject: BehaviorSubject<Image>;
    albumTitle: string;
    album: Album;
    image: Image;

    constructor(
        private authenticationService: AuthenticationService,
        private router: Router
    ) {
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
          this.currentUser = user;
          if (this.currentUser == null) {
            this.router.navigate(['/login']);
          }
      });
      // Getting current items from local storage
      this.currentAlbumSubject = new BehaviorSubject<Album>(JSON.parse(localStorage.getItem('selectedAlbum')));
      this.currentImageSubject = new BehaviorSubject<Image>(JSON.parse(localStorage.getItem('selectedImage')));
  }

  ngOnInit() {
    this.currentAlbumSubject.subscribe((data: Album) => {
      this.albumTitle = data.title;
      this.images = data.images;
      this.album = data;
    });
    this.currentImageSubject.subscribe((data: Image) => {
      this.image = data;
    });
  }

  ngOnDestroy() {
      // Unsubscribe to ensure no memory leaks
      this.currentUserSubscription.unsubscribe();
  }

  backClicked() {
    this.router.navigate(['album/']);
  }

  // Allows navigation beetwen images by adding +1 or -1 depending on direction
  plusSlides(n) {
    const id = this.image.id + n;
    // Checks if next or prev image is available, otherwise shows first or last depending on direction
    // In case we are on last image next will be 1. image, if we are on first image previous will be last image
    if (this.album.images.filter(x => x.id === id).length > 0) {
      this.image = this.album.images.filter(x => x.id === id)[0];
    } else {
      if (n === 1) {
        this.image = this.album.images[0];
      } else {
        this.image = this.album.images[this.album.images.length - 1];
      }
    }
  }
}
