import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { Album, Image } from './../../models/albums';
import { Component, OnInit, OnDestroy, AfterViewInit, HostListener, Input } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { User } from '../../models';
import { AuthenticationService } from '../../services';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

@Component({ templateUrl: 'album.component.html', styleUrls: ['album.component.scss'] })
export class AlbumComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    layout = 'grid';
    images: Image[] = [];
    displayImages: Image[] = [];
    private currentAlbumSubject: BehaviorSubject<Album>;
    albumTitle: string;
    album: Album;
    albumFound: string;

    // HostListener that listen for scroll event and loading new items
    @HostListener('window:scroll', [])
    onScroll(): void {
      if (this.bottomReached()) {
        this.displayImages = this.images.slice(0, this.displayImages.length + 3);
      }
    }
    // Check if bottom of screen is reached
    bottomReached(): boolean {
      const elmnt = document.getElementById('main-div');
      return (window.innerHeight + window.scrollY) >= elmnt.offsetHeight;
    }

    constructor(
        private authenticationService: AuthenticationService,
        private router: Router,
        private dialog: MatDialog
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
            // If user is not logged in redirect to Login page
            if (this.currentUser == null) {
              this.router.navigate(['/login']);
            }
        });
        this.currentAlbumSubject = new BehaviorSubject<Album>(JSON.parse(localStorage.getItem('selectedAlbum')));
    }

    ngOnInit() {
      this.currentAlbumSubject.subscribe((data: Album) => {
          this.albumTitle = data.title;
          this.images = data.images;
          this.displayImages = this.images.slice(0, 6);
          this.album = data;
      });
    }

    ngOnDestroy() {
        // Unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    // Search functionality
    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      if (filterValue.length > 0) {
          this.displayImages = this.images.filter(x => x.title.includes(filterValue));
          // Check if there is images to display
          if (this.displayImages.length === 0) {
            this.albumFound = 'No Images found!';
          }
          // Reset array when search field is empty
      } else {
          this.displayImages = this.images.slice(0, 6);
      }
    }

    backClicked() {
      this.router.navigate(['/']);
    }

    // Store selected image to localStorage and navigate to that image
    imageSelected(image: Image) {
        localStorage.setItem('selectedImage', JSON.stringify(image));
        this.router.navigate(['/image']);
    }

    // Open delete confirmation dialog and define its properties
    openDialog(item: Image, view: string) {
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        data: {
          message: 'Are you sure that you want to delete this image?',
          buttonText: {
            ok: 'Confirm',
            cancel: 'Cancel'
          }
        }
      });

      dialogRef.afterClosed().subscribe((confirmed: boolean) => {
        // Action on delete confirmed
        if (confirmed) {
          // Remove image from array of displayed images
          this.displayImages = this.displayImages.filter(obj => obj !== item);
          // Remove image from array of all images
          this.images = this.images.filter(obj => obj !== item);
          // Add new images to displayed images so scroll is still possible
          if (this.displayImages.length < 4 && view === 'grid') {
            this.displayImages = this.images.slice(0, 6);
          }
          if (this.displayImages.length < 6 && view === 'list') {
            this.displayImages = this.images.slice(0, 6);
          }
        }
      });
    }
}
