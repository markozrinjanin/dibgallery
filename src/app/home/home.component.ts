import { Album } from './../models/albums';
import { Component, OnInit, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { User } from '../models';
import { UserService, AuthenticationService } from '../services';
import { AlbumService } from '../services/album.services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({ templateUrl: 'home.component.html', styleUrls: ['home.component.scss'] })
export class HomeComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    layout = 'grid';
    albums: Album[] = [];
    displayAlbums: Album[] = [];
    loading = false;
    private currentAlbumSubject: BehaviorSubject<Album[]>;
    albumFound: string;

    constructor(
        private authenticationService: AuthenticationService,
        private albumService: AlbumService,
        private userService: UserService,
        private router: Router
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this.currentAlbumSubject = new BehaviorSubject<Album[]>(JSON.parse(localStorage.getItem('currentAlbum')));
    }
    // HostListener that listen for scroll event and loading new items
    @HostListener('window:scroll', [])
    onScroll(): void {
      if (this.bottomReached()) {
        this.displayAlbums = this.albums.slice(0, this.displayAlbums.length + 3);
      }
    }

    // Check if bottom of screen is reached
    bottomReached(): boolean {
      const elmnt = document.getElementById('main-div');
      return (window.innerHeight + window.scrollY) >= elmnt.offsetHeight;
    }

    ngOnInit() {
        this.loadAllAlbums();
        this.loadAllUsers();
    }

    ngOnDestroy() {
        // Unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    // Search functionality filter display array if search text is contained in title or created field
    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        if (filterValue.length > 0) {
            this.displayAlbums = this.albums.filter(x => x.title.includes(filterValue) || x.created.includes(filterValue));
            // Check if there is images to display
            if (this.displayAlbums.length === 0) {
                this.albumFound = 'No Albums found!';
              }
        } else {
            this.displayAlbums = this.albums.slice(0, 6);
        }
    }

    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
        });
    }

    private loadAllAlbums() {
        this.albumService.getAll().pipe(first()).subscribe(albums => {
            this.albums = albums.filter(x => x.userId === this.currentUser.id);
            this.displayAlbums = this.albums.slice(0, 6);
            localStorage.setItem('currentAlbum', JSON.stringify(this.albums));
            this.currentAlbumSubject.next(this.albums);
        });
    }

    // Store selected album to localStorage and navigate to that album
    albumSelected(album: Album){
        this.router.navigate(['/album']);
        localStorage.setItem('selectedAlbum', JSON.stringify(album));

    }
}
