export interface Album {
  userId: number;
  id: number;
  title: string;
  images: Image[];
  created: string;
}

export interface Albums {
  albums: Album[];
}

export interface Image {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}
