import { Album, Albums, Image } from './../models/albums';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { User } from '../models';

// array in local storage for registered users
let users = [];
let albums = [];
let images: Image[] = [];

fetch('https://jsonplaceholder.typicode.com/users')
  .then((response) => response.json())
  .then((json) => users = json);

fetch('https://jsonplaceholder.typicode.com/albums')
  .then((response) => response.json())
  .then((json) => albums = json);

fetch('https://jsonplaceholder.typicode.com/photos')
  .then((response) => response.json())
  .then((json) => images = json);

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/users/authenticate') && method === 'POST':
                    return authenticate();
                case url.endsWith('/users/register') && method === 'POST':
                    return register();
                case url.endsWith('/users') && method === 'GET':
                    return getUsers();
                case url.endsWith('/albums') && method === 'GET':
                    return getAlbums();
                case url.match(/\/users\/\d+$/) && method === 'DELETE':
                    return deleteUser();
                default:
                    return next.handle(request);
            }
        }

        // route functions

        function authenticate() {
            const { username, password } = body;
            const user = users.find(x => x.username === username && x.username === password);
            if (!user) return error('Username or password is incorrect');
            return ok({
                id: user.id,
                username: user.username,
                name: user.name,
                token: 'fake-jwt-token'
            });
        }

        function register() {
            const user = body;
            if (users.find(x => x.username === user.username)) {
                return error('Username "' + user.username + '" is already taken')
            }

            user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            users.push(user);
            localStorage.setItem('users', JSON.stringify(users));

            return ok();
        }

        function getUsers() {
            if (!isLoggedIn()) { return unauthorized(); }
            return ok(users);
        }

        function deleteUser() {
            if (!isLoggedIn()) { return unauthorized(); }

            users = users.filter(x => x.id !== idFromUrl());
            localStorage.setItem('users', JSON.stringify(users));
            return ok();
        }

        // helper functions

        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message) {
            return throwError({ error: { message } });
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }

        function getAlbums() {
            const imageArray = getImages();
            const userArray = getAllUsers();
            const albumArray: Album[] = [];

            if (!isLoggedIn()) { return unauthorized(); }
            albums.forEach((album, index) => {
                albumArray.push({
                    id: album.id,
                    title: album.title,
                    userId: album.userId,
                    images: imageArray.filter(x => x.albumId === album.id),
                    created: userArray[getRandomInt(9)].name
                });
            });
            return ok(albumArray);
        }

        function getRandomInt(max) {
            return Math.floor(Math.random() * Math.floor(max));
          }

        function getAllUsers(): User[] {
            return users as [];
        }

        function getImages(): Image[] {
            return images as [];
        }

    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
