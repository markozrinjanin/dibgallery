import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './services';
import { User } from './models';

@Component({ selector: 'app', templateUrl: 'app.component.html', styleUrls: ['app.component.scss']
})
export class AppComponent {
    currentUser: User;
    title = 'DibGalleryApp';

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    logout() {
        // Removing items stored in locale storage
        this.authenticationService.logout();
        // Redirecting to login
        this.router.navigate(['/login']);
    }
}
